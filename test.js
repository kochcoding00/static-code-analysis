const main = require("./src/main");

test('odd', () => {
    expect(main.checkOdd(1)).toBe(true);
});

test('sum', () => {
    expect(main.sum(1, 2)).toBe(3);
});