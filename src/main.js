const isOdd = require('is-odd');

// this function is tested
function checkOdd(num) {
    return isOdd(num);
} 

// this function is tested
function sum(x, y) {
    return x + y;
}

// this function is not tested
function mul(x, y) {
    return x * y;
}

exports.checkOdd = checkOdd;
exports.sum = sum;
exports.mul = mul;
